<?php

namespace App\Controller;

use App\Entity\Voiture;
use App\Repository\GarageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{

    /**
     * @var GarageRepository
     */
    private $voitRepo;

    public function __construct(GarageRepository $GarageRepository)
    {
        $this->voitRepo = $GarageRepository;
    }


    /**
     * @Route("/home", name="home", methods={"GET"})
     * @return Response
     */
    public function welcome()
    {
        // je déclare un tableau d'argument
        $tableau = [1, 2, "abc"];
        return $this->render("front/home.html.twig", [
            "tableau" => $tableau,
            "titre" => "KBS Garage", // je déclare une string
        ]);
//        return new Response(
//            "<html><head><title>Bienvenue</title></head><body></body></html>"
//        );
    }

    /**
     * @Route("/page/{numPage}", name="app_page", methods={"GET"}, requirements={"numPage"="\d+"})
     * @param string $numPage
     * @return Response
     */
    public function pages( int $numPage = 1 )
    {
      return $this->render("front/page.html.twig", [
         "numPage" => $numPage,
      ]);
    }

//    public function pageDeux()
//    {
//        return new Response(
//            "<html><head><title>Page2</title></head><body><h2>Vous êtes sur la page 2</h2><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium alias commodi deserunt eius eligendi esse facere, impedit labore nemo placeat repudiandae tenetur, ut? Ea illo, molestiae officia quo similique voluptate.</p></body></html>"
//        );
//    }

    /**
     * @Route("/page/{slug}", name="app_slug", methods={"GET"})
     * @param string $slug
     * @return void
     */
    public function autrePage(string $slug)
    {
        return new Response(
            "<html>
                        <head>
                            <title>Page $slug</title>
                        </head>
                        <body>
                        <h2>Vous êtes sur la page $slug</h2>
                        <p><a href='/home'>retour</a> </p>
                        <p>Une autre page</p></body></html>"
        );
    }

    /**
     * @Route("/listeVoitures", name="listeVoitures", methods={"GET"})
     * @return void
     */
    public function listeVoitures()
    {

        $voitures = $this->voitRepo->findAll();
        return $this->render("front/template_part/_listeVoitures.html.twig",[
            'voitures' => $voitures,
        ]);
    }


     /**
     * @Route("voiture/{numVoi}", name="voiture", methods={"GET"}, requirements={"numVoi"="\d+"})
     * @param int $numVoi
     * @return Response
     */
    public function voiture(int $numVoi)
    {
        $voiture = $this->voitRepo->find($numVoi);
        return $this->render("front/voiture.html.twig",[
            'voiture'=> $voiture
        ]);
    }
}