<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin/home", name="homeAdmin")
     * @return void
     */
    public function homeAdmin()
    {
        return $this->render("back/homeAdmin.html.twig");
    }

    /**
     * @Route("/login", name="login")
     * @return void
     */
    public function login(AuthenticationUtils $authenticationUtils)
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastEmail = $authenticationUtils->getLastUsername();
        return $this->render("back/form/loginForm.html.twig",[
            'lastEmail' => $lastEmail,
            'error' => $error,
        ]);
    }


    /**
     * @Route("/logout", name="logout")
     * @return void
     */
    public function logOut()
    {
        $this->redirectToRoute("home");
    }
}