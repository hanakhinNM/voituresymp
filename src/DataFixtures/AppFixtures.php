<?php

namespace App\DataFixtures;

use App\Entity\Garage;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;

class AppFixtures extends Fixture
{
    /**
     * @var UserPasswordHasherInterface
     */
    private $pass_hasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->pass_hasher = $passwordHasher;
    }

    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);
        $faker = Faker\Factory::create("fr_FR");

                // génération d'utilisateur
                $userone = new User();
                $userone
                    ->setNom("Devi")->setPrenom("Savitri")
                    ->setEmail("vraieecolo@pascommetoi.com")
                    ->setPassword($this->pass_hasher->hashPassword($userone, "demo"));
                $manager->persist($userone);
        
                $usertwo = new User();
                $usertwo
                    ->setNom("Linkola")->setPrenom("Penti")
                    ->setEmail("vraiecolo@pascommetoi.com")
                    ->setPassword($this->pass_hasher->hashPassword($usertwo, "demo"));
                $manager->persist($usertwo);

                $userthree = new User();
                $userthree
                    ->setNom("Gobbels")->setPrenom("Joseph")
                    ->setEmail("cavacestpourire@lol.com")
                    ->setPassword($this->pass_hasher->hashPassword($userthree, "demo"));
                $manager->persist($usertwo);


        for ($i = 0; $i < 10; $i++)
        {
            $garage = new Garage();
            $garage -> setMarque($faker->name())
                    -> setCouleur($faker->colorName())
                    -> setPrix($faker->randomNumber(5,false))
                    -> setDescription($faker->paragraph(5,true));
            $manager ->persist($garage);        
                    
        }
        $manager->flush();

    }
}
